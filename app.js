const request = require('request');
const prompt = require('prompt')

/* flags to indicate if the notifications are sent or not */
let notify_normal = false;
let notify_outside = false;

const getCurrentTempartureByNameAPI = function (city_name) {
    return new Promise((resolve, reject) => {
        request.get({
            url: `http://api.openweathermap.org/data/2.5/find?q=${city_name}&units=metric&appid=c9d9c5a3e31e1a07f0224ce247667891`,
        }, function (error, response, body) {
            if (error) {
                return reject(new Error('internal server error'));
            }
            let res = JSON.parse(body);

            if(res.cod === 400){
                return reject(new Error('city name is not valid'));
            }
            if(!res.list){
                return reject(new Error('no data found for supplied city'));
            }
            return resolve(res);
        });
    })
}

/**
    function to get the current temperature by passing the city_name 
    @inputParams city_name name of the city 
**/
const getCurrentTempartureByName = function (city_name) {
    return new Promise((resolve, reject) => {
        return getCurrentTempartureByNameAPI(city_name)
            .then((response) => {
                let temp = response.list[0].main.temp;
                return resolve(temp);
            })
            .catch((error) => {
                return reject(error);
            })
    })
}

/**
    function to notify the user for the temperature change
    @inputParams curr_temp current temperature of the city
    @inputParams inRange flag to define if the current temperature in is the range provided by the user or not

**/
const notifyUserAPI = (curr_temp, inRange) => {
    return new Promise((resolve, reject) => {
        request.post({
            url: `https://reqres.in/api/notifyUser/1`,
            headers: 'content-type : application/json',
            body: JSON.stringify({ current_temperature: curr_temp })
        }, function (error, response, body) {
            if (error) {
                return reject(new Error('internal server error'));
            }
            if (!inRange) {
                console.info(`notified user for outside temperature range at ${new Date()}`);
                notify_outside = true;
                notify_normal = false;
            } else {
                console.info(`notified user for inside temperature range at ${new Date()}`);
                notify_outside = false;
                notify_normal = true;
            }
            return resolve(body);
        });
    })
}


const getTemperatureNotification = (city_name, min_temp, max_temp) => {
    return new Promise((resolve, reject) => {

        return getCurrentTempartureByName(city_name)
            .then((curr_temp) => {
                console.info(`current_temp of ${city_name} is ${curr_temp} degree celsius at ${new Date()}`);
                let inRange = false;
                if (curr_temp >= min_temp && curr_temp <= max_temp) {
                    if (!notify_normal) {
                        inRange = true;
                        return notifyUserAPI(curr_temp, inRange);
                    }
                    console.log('user is already notified for normal temperature,skipping notification')
                    return Promise.resolve(true);
                }
                else {
                    if (!notify_outside) {
                        return notifyUserAPI(curr_temp, inRange);
                    }
                    console.log('user is already notified for outside range temperature,skipping notification')
                    return Promise.resolve(true);
                }
            })
            .then((response) => {
                return resolve(response);
            })
            .catch((err) => {
                console.error(err.message);
               process.exit();
            })
    })
};



const TemperatureNotification = () => {
    // getting  user inputs
    prompt.start();

    let schema = {
        properties:{
            city: {
                description: 'Enter the name of city',
                required: true,
                message: 'city name is required and should be valid',
                pattern: /^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/
                
            },
            min_temp: {
                description: 'Enter the minimum temparture value in degree celsius',
                required: true,
                message: 'minimum temperatue value is required and must be a number',
                pattern: /^-?\d+(\.\d{1,2})?$/
            },
            max_temp: {
                description: 'Enter the maximum temparture value value in degree celsius',
                required: true,
                message: 'maximum temperatue value is required and must be a number',
                pattern: /^-?\d+(\.\d{1,2})?$/
            }
        }
    };
    prompt.get(schema, function (err, results) {
        if (err) {
            console.log('error getting user inputs');
            process.exit();
        }
          let city =  results.city;
          let min_temp  =  parseInt(results.min_temp);
          let max_temp  =  parseInt(results.max_temp);
        // validating inputs 
        if (min_temp <= -273.15 ||  min_temp> 1000) {
            console.error('Failed : Invalid minimum temperature value');
            process.exit();
        }

        if (  max_temp <= -273.15 || max_temp > 1000) {
            console.error('Failed : Invalid maximum temperature value');
            process.exit();
        }

        if ( min_temp >= max_temp) {
            console.error('Failed : minimum temperature value connot be more than or equal to maximam temperature value');
            process.exit();
        }
        
       console.info('application is now running, it will notify the user for the climate change after every 10 mins')
        getTemperatureNotification(city, min_temp, max_temp);

        //running the code after every 10 mins
        setInterval(function () {
            console.info(`checking the temperature at ${new Date()}`)
            getTemperatureNotification(city, min_temp,max_temp);
        }, 600000);
    })

}

TemperatureNotification();